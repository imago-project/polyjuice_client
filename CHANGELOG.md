0.3.0-pre
=====

Breaking changes:

- The `Polyjuice.Client.Storage` protocol adds a namespace parameter to the
  key-value storage functions.  Code that uses the function must add this
  parameter to their calls, and modules that implement the protocol must add
  this parameter to their implementations.  Existing stored data should be
  migrated.
- The `Polyjuice.Client.Storage` protocol adds a `kv_del` function.  Modules
  that implement the protocol must add this function to their implementations.

Features:

- Make `Polyjuice.Client.Filter.update/4` and `Polyjuice.Client.Filter.put/3`
  public.

0.2.1
=====

Features:

- Add a tutorial.

Bug fixes:

- Fix ETS storage so that the sync process can write to it.

0.2.0
=====

Breaking changes:

- The `Polyjuice.Client` struct has some extra fields that are required by some
  functions.
- The `Polyjuice.Client.API.sync_child_spec` function no longer takes a storage
  as a separate argument -- it is passed in as part of the client.
- Functions for sending events were moved from `Polyjuice.Client` to
  `Polyjuice.Client.Room`, and the order of arguments was changed.

Features:

- New methods and endpoint for setting filters, as well as support in sync.
- Sync sends more messages to update on more of the sync result.
- New function for joining rooms.

0.1.0
====

Initial release.
