# Copyright 2020 Hubert Chathi <hubert@uhoreg.ca>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

defmodule Polyjuice.Client.Endpoint.GetRoomsMessages do
  @moduledoc """
  Paginates events.

  https://matrix.org/docs/spec/client_server/latest#get-matrix-client-r0-rooms-roomid-messages
  """

  @type t :: %__MODULE__{
          room: String.t(),
          from: String.t(),
          to: String.t() | nil,
          dir: :forward | :backward,
          limit: integer() | nil,
          filter: map() | nil
        }

  @enforce_keys [:room, :from, :dir]
  defstruct [
    :room,
    :from,
    :to,
    :dir,
    :limit,
    :filter
  ]

  defimpl Polyjuice.Client.Endpoint.Proto do
    def http_spec(
          req,
          base_url
        ) do
      e = &URI.encode_www_form/1

      query =
        [
          [
            {"from", req.from},
            case req.dir do
              :forward -> {"dir", "f"}
              :backward -> {"dir", "b"}
            end
          ],
          if(req.to, do: [{"to", req.to}], else: []),
          if(req.limit, do: [{"limit", req.limit}], else: []),
          if(req.filter, do: [{"filter", Poison.encode!(req.filter)}], else: [])
        ]
        |> Enum.concat()
        |> URI.encode_query()

      url = %{
        URI.merge(
          base_url,
          "#{Polyjuice.Client.prefix_r0()}/rooms/#{e.(req.room)}/messages"
        )
        | query: query
      }

      %Polyjuice.Client.Endpoint.HttpSpec{
        method: :get,
        headers: [
          {"Accept", "application/json"}
        ],
        url: to_string(url)
      }
    end

    def transform_http_result(_req, status_code, _resp_headers, body) do
      case status_code do
        200 ->
          {:ok, Poison.decode!(body)}

        _ ->
          {:error, status_code, body}
      end
    end
  end
end
