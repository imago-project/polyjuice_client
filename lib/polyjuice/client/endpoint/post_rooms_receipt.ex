# Copyright 2019 Hubert Chathi <hubert@uhoreg.ca>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

defmodule Polyjuice.Client.Endpoint.PostRoomsReceipt do
  @moduledoc """
  Update the user's read receipt.

  https://matrix.org/docs/spec/client_server/r0.5.0#post-matrix-client-r0-rooms-roomid-receipt-receipttype-eventid
  """

  @type t :: %__MODULE__{
          room: String.t(),
          event_id: String.t(),
          receipt_type: String.t()
        }

  @enforce_keys [:room, :event_id]

  defstruct [
    :room,
    :event_id,
    receipt_type: "m.read"
  ]

  defimpl Polyjuice.Client.Endpoint.Proto do
    def http_spec(
          %Polyjuice.Client.Endpoint.PostRoomsReceipt{
            room: room,
            event_id: event_id,
            receipt_type: receipt_type
          },
          base_url
        )
        when is_binary(base_url) do
      e = &URI.encode_www_form/1

      %Polyjuice.Client.Endpoint.HttpSpec{
        method: :post,
        headers: [
          {"Accept", "application/json"},
          {"Content-Type", "application/json"}
        ],
        url:
          URI.merge(
            base_url,
            "#{Polyjuice.Client.prefix_r0()}/rooms/#{e.(room)}/receipt/#{e.(receipt_type)}/#{
              e.(event_id)
            }"
          )
          |> to_string(),
        body: "{}"
      }
    end

    def transform_http_result(_req, status_code, _resp_headers, body) do
      case status_code do
        200 ->
          {:ok}

        _ ->
          {:error, status_code, body}
      end
    end
  end
end
