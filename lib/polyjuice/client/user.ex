# Copyright 2019 Hubert Chathi <hubert@uhoreg.ca>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

defmodule Polyjuice.Client.User do
  @moduledoc """
  User-related functions.
  """

  @doc """
  Log in with a password.

  `client_or_url` may be either something that implements `Polyjuice.Client.API`
  or a homeserver base URL.

  `identifier` may be a single string (in which case it represents a username
  -- either just the localpart or the full MXID), a tuple of the form
  `{:email, "email@address"}`, a tuple of the form `{:phone, "country_code",
  "phone_number"}`, or a map that is passed directly to the login endpoint.

  `opts` is a keyword list of options:

  - `device_id:` (string) the device ID to use
  - `initial_device_display_name:` (string) the display name to use for the device
  """
  @spec log_in_with_password(
          client_or_url :: Polyjuice.Client.API.t() | String.t(),
          identifier :: String.t() | tuple() | map(),
          password :: String.t(),
          opts :: list()
        ) :: {:ok, map()} | Any
  def log_in_with_password(client_or_url, identifier, password, opts \\ [])
      when (is_binary(identifier) or is_tuple(identifier) or is_map(identifier)) and
             is_binary(password) and is_list(opts) do
    client_api =
      if is_binary(client_or_url) do
        %Polyjuice.Client{
          base_url: client_or_url
        }
      else
        client_or_url
      end

    id =
      case identifier do
        x when is_binary(x) ->
          %{
            "type" => "m.id.user",
            "user" => identifier
          }

        {:email, address} ->
          %{
            "type" => "m.id.thirdparty",
            "medium" => "email",
            "address" => address
          }

        {:phone, country, phone} ->
          %{
            "type" => "m.id.phone",
            "country" => country,
            "phone" => phone
          }

        x when is_map(x) ->
          identifier
      end

    Polyjuice.Client.API.call(
      client_api,
      %Polyjuice.Client.Endpoint.PostLogin{
        type: "m.login.password",
        identifier: id,
        password: password,
        device_id: Keyword.get(opts, :device_id),
        initial_device_display_name: Keyword.get(opts, :initial_device_display_name)
      }
    )
  end

  @doc """
  Log out an existing session.
  """
  @spec log_out(client_api :: Polyjuice.Client.API.t()) :: {:ok} | Any
  def log_out(client_api) do
    Polyjuice.Client.API.call(
      client_api,
      %Polyjuice.Client.Endpoint.PostLogout{}
    )
  end
end
