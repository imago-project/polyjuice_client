# Copyright 2020 Hubert Chathi <hubert@uhoreg.ca>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

defmodule Polyjuice.Client.UserTest do
  use ExUnit.Case

  test "log in" do
    with client = %DummyClient{
           response: {
             %Polyjuice.Client.Endpoint.PostLogin{
               type: "m.login.password",
               identifier: %{
                 "type" => "m.id.user",
                 "user" => "user"
               },
               password: "password"
             },
             {:ok, %{}}
           }
         } do
      {:ok, %{}} = Polyjuice.Client.User.log_in_with_password(client, "user", "password")
    end

    with client = %DummyClient{
           response: {
             %Polyjuice.Client.Endpoint.PostLogin{
               type: "m.login.password",
               identifier: %{
                 "type" => "m.id.thirdparty",
                 "medium" => "email",
                 "address" => "user@example.com"
               },
               password: "password",
               device_id: "device_id",
               initial_device_display_name: "Device name"
             },
             {:ok, %{}}
           }
         } do
      {:ok, %{}} =
        Polyjuice.Client.User.log_in_with_password(
          client,
          {:email, "user@example.com"},
          "password",
          device_id: "device_id",
          initial_device_display_name: "Device name"
        )
    end

    with client = %DummyClient{
           response: {
             %Polyjuice.Client.Endpoint.PostLogin{
               type: "m.login.password",
               identifier: %{
                 "type" => "m.id.phone",
                 "country" => "CA",
                 "phone" => "1234567890"
               },
               password: "password"
             },
             {:ok, %{}}
           }
         } do
      {:ok, %{}} =
        Polyjuice.Client.User.log_in_with_password(
          client,
          {:phone, "CA", "1234567890"},
          "password"
        )

      {:ok, %{}} =
        Polyjuice.Client.User.log_in_with_password(
          client,
          %{
            "type" => "m.id.phone",
            "country" => "CA",
            "phone" => "1234567890"
          },
          "password"
        )
    end
  end

  test "log out" do
    with client = %DummyClient{
           response: {
             %Polyjuice.Client.Endpoint.PostLogout{},
             {:ok}
           }
         } do
      {:ok} = Polyjuice.Client.User.log_out(client)
    end
  end
end
