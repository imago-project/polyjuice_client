# Copyright 2019 Hubert Chathi <hubert@uhoreg.ca>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

defmodule Polyjuice.Client.Endpoint.PostRoomsReceiptTest do
  use ExUnit.Case

  test "POST rooms/{room_id}/receipt/{receipt_type}/{event_id}" do
    endpoint = %Polyjuice.Client.Endpoint.PostRoomsReceipt{
      room: "!room_id",
      event_id: "$event_id"
    }

    http_spec = Polyjuice.Client.Endpoint.Proto.http_spec(endpoint, "https://example.com")

    assert http_spec == %Polyjuice.Client.Endpoint.HttpSpec{
             auth_required: true,
             body: "{}",
             headers: [
               {"Accept", "application/json"},
               {"Content-Type", "application/json"}
             ],
             method: :post,
             url:
               "https://example.com/_matrix/client/r0/rooms/%21room_id/receipt/m.read/%24event_id"
           }

    assert Polyjuice.Client.Endpoint.Proto.transform_http_result(
             endpoint,
             200,
             [],
             "{}"
           ) == {:ok}

    assert Polyjuice.Client.Endpoint.Proto.transform_http_result(
             endpoint,
             500,
             [],
             "Aaah!"
           ) == {:error, 500, "Aaah!"}
  end
end
