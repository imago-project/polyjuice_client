# Copyright 2020 Hubert Chathi <hubert@uhoreg.ca>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

defmodule Polyjuice.Client.Endpoint.GetSyncTest do
  use ExUnit.Case

  test "GET sync" do
    with endpoint = %Polyjuice.Client.Endpoint.GetSync{} do
      http_spec = Polyjuice.Client.Endpoint.Proto.http_spec(endpoint, "https://example.com")

      assert http_spec == %Polyjuice.Client.Endpoint.HttpSpec{
               auth_required: true,
               body: "",
               headers: [
                 {"Accept", "application/json"}
               ],
               method: :get,
               url: "https://example.com/_matrix/client/r0/sync?timeout=0"
             }

      assert Polyjuice.Client.Endpoint.Proto.transform_http_result(
               endpoint,
               200,
               [],
               "{}"
             ) == {:ok, %{}}

      assert Polyjuice.Client.Endpoint.Proto.transform_http_result(
               endpoint,
               500,
               [],
               "Aaah!"
             ) == {:error, 500, "Aaah!"}
    end

    with endpoint = %Polyjuice.Client.Endpoint.GetSync{
           filter: "1",
           since: "token",
           full_state: true,
           set_presence: :offline,
           timeout: 120
         } do
      http_spec = Polyjuice.Client.Endpoint.Proto.http_spec(endpoint, "https://example.com")

      assert http_spec == %Polyjuice.Client.Endpoint.HttpSpec{
               auth_required: true,
               body: "",
               headers: [
                 {"Accept", "application/json"}
               ],
               method: :get,
               url:
                 "https://example.com/_matrix/client/r0/sync?timeout=120&since=token&full_state=true&filter=1&set_presence=offline"
             }
    end

    with endpoint = %Polyjuice.Client.Endpoint.GetSync{
           filter: %{},
           set_presence: :unavailable
         } do
      http_spec = Polyjuice.Client.Endpoint.Proto.http_spec(endpoint, "https://example.com")

      assert http_spec == %Polyjuice.Client.Endpoint.HttpSpec{
               auth_required: true,
               body: "",
               headers: [
                 {"Accept", "application/json"}
               ],
               method: :get,
               url:
                 "https://example.com/_matrix/client/r0/sync?timeout=0&filter=%7B%7D&set_presence=unavailable"
             }
    end
  end
end
