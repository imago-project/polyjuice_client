# Polyjuice Client

Polyjuice Client is a [Matrix](https://matrix.org/) client library.

## Installation

The package can be installed by adding `polyjuice_client` to your list of
dependencies in `mix.exs`:

```elixir
def deps do
  [
    {:polyjuice_client, "~> 0.1.0"}
  ]
end
```

## Usage

To use this library, start by looking at the the
[tutorial](tutorial_echo.html), or the documentation for `Polyjuice.Client`.
